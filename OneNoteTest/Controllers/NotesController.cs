﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OneNoteTest.Helpers;
using OneNoteTest.Models;

namespace OneNoteTest.Controllers
{
    [Route("api/[controller]")]
    public class NotesController : Controller
    {
        private  NoteContext _context;

        public NotesController(NoteContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<NoteList>> Get()
        {
            return await _context.NotesContext.Include(notes => notes.Notes).ToListAsync();
        }
        [HttpPost("[action]")]
        public IActionResult AddGroup([FromBody] string name)
        {
            if (string.IsNullOrEmpty(name)) return NoContent();
            var newGroup = new NoteList(){Name = name, Notes = new List<Note>()};
            _context.NotesContext.Add(newGroup);
            _context.SaveChanges();
            
            return new JsonResult(newGroup.Id);
        }

        [HttpPost("[action]")]
        public IActionResult AddNote([FromBody] Note note)
        {
            if (note.ListId == 0 || string.IsNullOrEmpty(note.Title)) return NoContent();

            var list = _context.NotesContext.FirstOrDefault(group => group.Id == note.ListId);
            if (list == null) return NoContent();
            if(list.Notes == null)
                list.Notes = new List<Note>();
            list.Notes.Add(note);
            _context.SaveChanges();

            return Ok();
        }
   
    }
}
