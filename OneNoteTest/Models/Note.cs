﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OneNoteTest.Models
{
    public class Note
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int ListId { get; set; }
        [JsonIgnore]
        public NoteList NoteList { get; set; }
    }
}
