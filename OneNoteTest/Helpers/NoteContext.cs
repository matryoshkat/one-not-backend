﻿using Microsoft.EntityFrameworkCore;
using OneNoteTest.Models;

namespace OneNoteTest.Helpers
{
    public class NoteContext : DbContext
    {

        public DbSet<NoteList> NotesContext { get; set; }

        public NoteContext(DbContextOptions<NoteContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

    }
}
